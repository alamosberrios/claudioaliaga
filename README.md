# Ejecicio Práctico

```
1.- Clonar el siguiente proyecto: 
	git clone https://alamosberrios@bitbucket.org/alamosberrios/claudioaliaga.git

2.-
	2.1- Trabajar en la carpeta terraform:
		- Desarrollar un código que permita deployar lo siguiente:
			- GCE
			- GCS (Bucket) (no será usado, sólo deployado)
			- Usar la VPC default
			- Levantar un servidor web (De tu preferencia) que permita visualizar un Hello World, en uno de los puertos con tráfico permitido.

3.-	Crear un Jenkinsfile con 1 stage y 3 steps dentro de la carpeta jenkinsfile que permita lo siguiente:
	-Step 1: Testear conexión a Google
	-Step 2: Listar bucket creado
	-Step 3: Imprimir un Hello World

4.-	Crear un Dockerfile que permita lo siguiente:
	- Levantar 1 servidor jenkins desde https://hub.docker.com/ y setear un proxy ficticio(Puedes inventar una IP, pero debe quedar seteado el proxy).


```


####	NOTA: Considerar crear una nueva rama para trabajar, y cuándo estés listo para aplicar los cambios, solicitar un pull request a la rama master. Si gustas puedes realizar el Merge :)

# Cualquier pregunta, acercate a nuestros puestos :)
