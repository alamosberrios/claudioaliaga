set -e
set -ua

#Crea un proyecto
#Crea la cuanta de servicio iam
#Genera la Key para luego poder levantar una VM generica con el archivo main.tf
#Habilita las APIs basicas para poder ejecutar instrucciones
#Crea el bucket


#falto configurar el billing
#faltan los grant

. ./setup_project.env

SA_ID="$SA_NAME@$SEED_PROJECT.iam.gserviceaccount.com"

echo "Chekinging org ID..."
CHECK_ORG_ID="$(gcloud organizations list --format="value(ID)" --filter="$ORG_ID")"

echo "Checking project..."
CHECK_SEED_PROJECT="$(gcloud projects list --format="value(projectId)" --filter="$SEED_PROJECT")"

if [[ $CHECK_SEED_PROJECT == "" ]];
then
   echo "Project does not exist."
   gcloud projects create $SEED_PROJECT
   echo "Project created."
else
   echo "Project exists."
fi

echo "========================================================================"
echo "Create Service Account"
echo "========================================================================"
CHECK_SA="$(gcloud iam service-accounts list --format="value(NAME)" --filter="${SA_NAME}" --project=${SEED_PROJECT})"
if [[ $CHECK_SA == "" ]];
then
   echo "Service Account '$SA_NAME' does not exist."
   gcloud iam service-accounts \
    --project "${SEED_PROJECT}" create ${SA_NAME} \
    --display-name ${SA_NAME}
   echo "Service account created."
else
   echo "Seed Service Account '$SA_NAME' exists."
fi

echo "========================================================================"
echo "Get Key"
echo "========================================================================"
echo "Moving to $KEY_FILE..."
gcloud iam service-accounts keys create "${KEY_FILE}" \
    --iam-account "${SA_ID}" \
    --user-output-enabled false
echo "Key moved."


echo "========================================================================"
echo "Enable APIs"
echo "========================================================================"
gcloud services enable \
  cloudresourcemanager.googleapis.com \
  --project "${SEED_PROJECT}"
echo "cloudresourcemanager Done."


gcloud services enable \
  iam.googleapis.com \
  --project "${SEED_PROJECT}"
echo "iam Done."


gcloud services enable \
  admin.googleapis.com \
  --project "${SEED_PROJECT}"
echo "admin Done."


gcloud services enable \
  sourcerepo.googleapis.com \
  --project "${SEED_PROJECT}"
echo "sourcerepo Done."


gcloud services enable \
  storage-api.googleapis.com \
  --project "${SEED_PROJECT}"
echo "storage Done."

echo "========================================================================"
echo "Create GCS bucket"
echo "========================================================================"
if [ "x$TF_BUCKET_NAME" != "x"  ]; then

  CHECK_BUCKET="$(gsutil ls -b -p $SEED_PROJECT gs://${TF_BUCKET_NAME}  2>/dev/null || echo '')"
  if [[ $CHECK_BUCKET == "" ]]; then
    gsutil mb -p $SEED_PROJECT "gs://${TF_BUCKET_NAME}"
  else
    echo "Bucket exists."
  fi
else
  echo "TF_BUCKET_NAME variable not set in input variables file. Exiting..."
  exit 1;
fi

echo "Done!!!"
