provider "google" {
	credentials = "/home/claudioaliaga/Documentos/homework/claudioaliaga/terraform/credentials.json"
	project = "test-gce-terraform"
	zone = "us-central1-c"
}

resource "google_compute_instance" "default" {
	name = "claudioaliaga-instance"
	machine_type = "f1-micro"
	zone = "us-central1-c"

	boot_disk {
		initialize_params {
			image = "debian-cloud/debian-9"
		}
	}

	metadata_startup_script = "sudo apt-get update && sudo apt-get install apache2 -y && echo '<!doctype html><html><body><h1>Hello World by Claudio Aliaga</h1></body></html>' | sudo tee /var/www/html/index.html"

	network_interface {
		#vpc por default
		network = "default"
		access_config {
		}
	}
	tags = ["http-server"]
}

resource "google_compute_firewall" "http-server" {
	name = "default-allow-http"
	network = "default"

	allow {
		protocol = "tcp"
		ports = ["80"]
	}

	source_ranges = ["0.0.0.0/0"]
	target_tags = ["http-server"]
}
